<?php

namespace App\Tests\Calculator;

use App\Calculator\SimpleCalculator;
use PHPUnit\Framework\TestCase;

class SimpleCalculatorTest extends TestCase
{
    public function testAdd()
    {
        $calculator = new SimpleCalculator();
        $result = $calculator->add(12, 15);

        $this->assertEquals(27, $result);
    }

    public function testAddRandomPositive()
    {
        $calculator = new SimpleCalculator();
        $var1 = mt_rand(1, pow(2, 15));
        $var2 = mt_rand(1, pow(2, 15));
        $result = $calculator->add($var1, $var2);

        $this->assertGreaterThan(0, $result);
    }
}
